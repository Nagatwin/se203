#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

// In .text or .rodata
const int32_t const_i32 = 8;
const char const_char_tab[] = "Hello world";

// In .data
int32_t i32_init = 8;

// In .bss
int32_t i32_not_init;

// In .text
void random_func();

int main()
{
	// In .data
	static int32_t i32_local_static_init = 8;

	// In .bss
	static int32_t i32_local_static_not_init;

	i32_local_static_not_init = 8;	
	
	// In stack
	int32_t i32_local = 8;

        printf("%p : text : random_func\n",&random_func);
	printf("%p : text/rodata : const_i32\n",&const_i32);
	printf("%p : text/rodata : const_char_tab\n",const_char_tab);
	printf("%p : data : i32_init\n",&i32_init);
	printf("%p : data : i32_local_static_init\n",&i32_local_static_init);
	printf("%p : bss : i32_local_static_not_init\n",&i32_local_static_not_init);
	printf("%p : bss : i32_not_init\n",&i32_not_init);

	// Add to heap
	int*  ptrs[5];
	
	//printf("Let's add some vars of size %ld to the heap\n",sizeof(int));

	for (int i =0; i != 5; i++){
		ptrs[i] = malloc(sizeof(int));
		printf("%p : heap  : ptrs[%d]\n",ptrs[i],i);
	}

	// Add to stack
	int*  ptrs2[5];

	//printf("Let's add some vars of size %ld to the stack\n",sizeof(int));

	for (int i = 0; i != 5; i++){
		ptrs2[i] = alloca(sizeof(int));
	}
        
	for (int i = 4; i != -1; i--){
        	printf("%p : stack : ptrs2[%d]\n",ptrs2[i],i);
        }
 
	printf("%p : stack : i32_local\n",&i32_local);

	// Free
	for(int i = 0; i != 5; i++){
		free(ptrs[i]);
	}
	
	return 0;
}

void random_func(){
	int i = 3;
	i ++;
	return;
}
