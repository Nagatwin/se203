# Exercice 2 :
## Recherches :

J'ai crée un fichier Hello world

J'ai utilisé objdump -h pour montrer les emplacement des sections (déterminées implicitement selon la doc : https://sourceware.org/binutils/docs/binutils/objdump.html )

On obtient :

> 13 .text         00000392  0000000000000630  0000000000000630  00000630  2**4 CONTENTS, ALLOC, LOAD, READONLY, CODE  
15 .rodata       00000146  00000000000009d0  00000000000009d0  000009d0  2**3 CONTENTS, ALLOC, LOAD, READONLY, DATA  
22 .data         00000018  0000000000201000  0000000000201000  00001000  2**3 CONTENTS, ALLOC, LOAD, DATA  
23 .bss          00000010  0000000000201018  0000000000201018  00001018  2**2 ALLOC



J'ai ensuite utilisé un fichier plus rempli avec :

- 2 variables globales constantes dans .rodata
- 1 fonction dans .text
- 1 variable globale initialisée dans data
- 1 variable globale non initialisée dans bss
- 1 variable locale initialisée dans la stack
- 5 allocations dans la stack
- 5 allocations dans le heap
- 1 variable locale static initialisée dans data
- 1 variable locale static non initialisée dans bss

On obtient alors le résultat suivant (compilation et execution de [code.c](code.c):

> 0x562dfa375931 : text : random_func  
0x562dfa3759d8 : text/rodata : const_i32  
0x562dfa3759e0 : text/rodata : const_char_tab  
0x562dfa576010 : data : i32_init  
0x562dfa576014 : data : i32_local_static_init  
0x562dfa57601c : bss : i32_local_static_not_init  
0x562dfa576020 : bss : i32_not_init  
0x562dfaaf0670 : heap  : ptrs[0]  
0x562dfaaf0690 : heap  : ptrs[1]  
0x562dfaaf06b0 : heap  : ptrs[2]  
0x562dfaaf06d0 : heap  : ptrs[3]  
0x562dfaaf06f0 : heap  : ptrs[4]  
0x7fffd369aac0 : stack : ptrs2[4]  
0x7fffd369aae0 : stack : ptrs2[3]  
0x7fffd369ab00 : stack : ptrs2[2]  
0x7fffd369ab20 : stack : ptrs2[1]  
0x7fffd369ab40 : stack : ptrs2[0]  
0x7fffd369ab6c : stack : i32_local

On a donc dans l'odre, avec Linux x86_64 (compilé avec gcc)
- La section text
- La section rodata
- La section data
- La section bss
- Le heap
Et plus loin, 
- La stack

On remarque aussi que la stack est allouée dans l'ordre décroissant des addresses
