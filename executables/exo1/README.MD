# Exercice 1 :
## Remarques :
- 4 correspond à la taille(en octet) du symbole y (un int 32)
- Tous les autres champs sont à 0

## Doc :
Selon la doc d'objdump (consultable ici par exemple : https://sourceware.org/binutils/docs/binutils/objdump.html )


> The other common output format, usually seen with ELF based files, looks like this:  
00000000 l    d  .bss   00000000 .bss  
00000000 g       .text  00000000 fred  

> Here the first number is the symbol’s value (sometimes refered to as its address). The next field is actually a set of characters and spaces indicating the flag bits that are set on the symbol. These characters are described below. Next is the section with which the symbol is associated or *ABS* if the section is absolute (ie not connected with any section), or *UND* if the section is referenced in the file being dumped, but not defined there.

Cela serait donc la 'valeur' du symbol (ou son adresse)

## Tests :
Pour tester j'ai modifié le .c avec des variables de différentes tailles (uint8, int8, int16, int23, int64)
Je le compile ensuite puis j'utilise objdump dessus pour voir la table des symboles (-t) [dump.txt](dump.txt)

On voit alors clairement que la 'valeur' du symbol global sans valeur est la même que sa taille.

On peut donc dire que c'est la taille du symbol qui est utilisée comme valeur.

