	.cpu arm7tdmi
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 1
	.eabi_attribute 30, 1
	.eabi_attribute 34, 0
	.eabi_attribute 18, 4
	.file	"exo3.c"
	.text
	.align	2
	.global	main
	.syntax unified
	.arm
	.fpu softvfp
	.type	main, %function
main:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	push	{r4, r5, r6, lr}
	sub	sp, sp, #8
	ldr	r6, .L3
	mov	r3, #12
	str	r3, [r6]
	ldr	r5, .L3+4
	ldrb	r4, [r5]	@ zero_extendqisi2
	add	r4, r4, #1
	and	r4, r4, #255
	strb	r4, [r5]
	ldr	r0, .L3+8
	bl	puts
	ldrb	r3, [r5]	@ zero_extendqisi2
	ldr	r2, [r6]
	ldr	r1, .L3+12
	ldr	r1, [r1]
	add	r4, r4, #12
	str	r4, [sp]
	ldr	r0, .L3+16
	bl	printf
	mov	r0, #0
	add	sp, sp, #8
	@ sp needed
	pop	{r4, r5, r6, lr}
	bx	lr
.L4:
	.align	2
.L3:
	.word	y
	.word	.LANCHOR0
	.word	.LC0
	.word	.LANCHOR1
	.word	.LC1
	.size	main, .-main
	.global	mesg
	.comm	y,4,4
	.global	x
	.section	.rodata
	.align	2
	.type	mesg, %object
	.size	mesg, 4
mesg:
	.word	.LC2
	.data
	.align	2
	.set	.LANCHOR1,. + 0
	.type	x, %object
	.size	x, 4
x:
	.word	34
	.bss
	.set	.LANCHOR0,. + 0
	.type	z.5126, %object
	.size	z.5126, 1
z.5126:
	.space	1
	.section	.rodata.str1.4,"aMS",%progbits,1
	.align	2
.LC0:
	.ascii	"Hello World!\000"
	.space	3
.LC1:
	.ascii	"x = %d, y = %d, z = %d, t = %d\012\000"
.LC2:
	.ascii	"Hello World!\012\000"
	.ident	"GCC: (15:6.3.1+svn253039-1build1) 6.3.1 20170620"
