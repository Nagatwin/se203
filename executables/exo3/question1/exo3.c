#include <stdint.h>
#include <stdio.h>

// .data
int32_t x = 34;

// .bss
int32_t y;

// .text/rodata
const char mesg[] = "Hello World!\n";

int main() {
  // .bss
  static uint8_t z;
  // stack
  uint16_t t;

  y = 12;
  z = z + 1;
  t = y+z;

  printf(mesg);
  printf("x = %d, y = %d, z = %d, t = %d\n", x, y, z, t);
  return 0;
}
