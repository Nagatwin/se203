#include "buttons.h"
#include "clocks.h"
#include "irq.h"
#include "led.h"
#include "matrix.h"
#include "uart.h"

int main() {
  // Init clocks
  clocks_init();

  // Init irq before uart (it uses interrupts)
  irq_init();

  // Init matrix
  matrix_init();

  // Init uart with the correct baudrate
  uart_init(38400);

  // Current threads goes to display the frame
  display_frame();
  return 0;
}
