#ifndef LED_H
#define LED_H
// LED_OFF : both leds OFF
// LED_YELLOW : Yellow ON, Blue OFF
// LED_BLUE : Blue ON, yellow OFF
typedef enum { LED_OFF, LED_YELLOW, LED_BLUE } state_t;

void led_init(void);
void led_g_on(void);
void led_g_off(void);
void led(state_t);
#endif
