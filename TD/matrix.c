#include "matrix.h"
#include "stm32l475xx.h"
#include "stm32l4xx.h"
#include <stdint.h>

// Macros
#define RST(x) SET_BIT(GPIOC->BSRR, x ? GPIO_BSRR_BS3 : GPIO_BSRR_BR3)
#define SB(x) SET_BIT(GPIOC->BSRR, x ? GPIO_BSRR_BS5 : GPIO_BSRR_BR5)
#define LAT(x) SET_BIT(GPIOC->BSRR, x ? GPIO_BSRR_BS4 : GPIO_BSRR_BR4)
#define SCK(x) SET_BIT(GPIOB->BSRR, x ? GPIO_BSRR_BS1 : GPIO_BSRR_BR1)
#define SDA(x) SET_BIT(GPIOA->BSRR, x ? GPIO_BSRR_BS4 : GPIO_BSRR_BR4)
#define ROW0(x) SET_BIT(GPIOB->BSRR, x ? GPIO_BSRR_BS2 : GPIO_BSRR_BR2)
#define ROW1(x) SET_BIT(GPIOA->BSRR, x ? GPIO_BSRR_BS15 : GPIO_BSRR_BR15)
#define ROW2(x) SET_BIT(GPIOA->BSRR, x ? GPIO_BSRR_BS2 : GPIO_BSRR_BR2)
#define ROW3(x) SET_BIT(GPIOA->BSRR, x ? GPIO_BSRR_BS7 : GPIO_BSRR_BR7)
#define ROW4(x) SET_BIT(GPIOA->BSRR, x ? GPIO_BSRR_BS6 : GPIO_BSRR_BR6)
#define ROW5(x) SET_BIT(GPIOA->BSRR, x ? GPIO_BSRR_BS5 : GPIO_BSRR_BR5)
#define ROW6(x) SET_BIT(GPIOB->BSRR, x ? GPIO_BSRR_BS0 : GPIO_BSRR_BR0)
#define ROW7(x) SET_BIT(GPIOA->BSRR, x ? GPIO_BSRR_BS3 : GPIO_BSRR_BR3)

// Make some space to store the frame
static rgb_color frameinit[64];
volatile rgb_color *frame = frameinit;

// Wait ns nanoseconds
static void wait(int ns) {
  // 80 000 000 * ns / (1 000 000 000 * 3 operations)
  for (int i = 0; i != ns / 50; i++) {
    asm volatile("nop");
  }
}

// Positive pulse on SCK pin
static void pulse_SCK() {
  SCK(0);
  wait(25);

  SCK(1);
  wait(25);

  SCK(0);
  wait(25);
}

// Negative pulse to LAT pin
static void pulse_LAT() {
  LAT(1);
  wait(25);

  LAT(0);
  wait(25);

  LAT(1);
  wait(25);
}

// Set all rows pins to 0
static void deactivate_rows() {
  ROW0(0);
  ROW1(0);
  ROW2(0);
  ROW3(0);
  ROW4(0);
  ROW5(0);
  ROW6(0);
  ROW7(0);
}

// Set row pin to 1
static void activate_row(const int row) {
  switch (row) {
  case 0:
    ROW0(1);
    break;
  case 1:
    ROW1(1);
    break;
  case 2:
    ROW2(1);
    break;
  case 3:
    ROW3(1);
    break;
  case 4:
    ROW4(1);
    break;
  case 5:
    ROW5(1);
    break;
  case 6:
    ROW6(1);
    break;
  case 7:
    ROW7(1);
    break;
  }
}

static void send_byte(const uint8_t val, const int bank) {
  // Select bank
  SB(bank);

  // Send the bits to bankes using SDA register
  for (int i = bank ? 7 : 5; i >= 0; i--) {
    SDA(val & (1 << i));
    pulse_SCK();
  }
}

// Set martix row
static void mat_set_row(const int row, volatile rgb_color *val) {
  // Send bytes for each led of the row
  for (int i = 7; i >= 0; i--) {
    send_byte(val[i].b, 1);
    send_byte(val[i].g, 1);
    send_byte(val[i].r, 1);
  }

  // Deactivate rows
  deactivate_rows();

  // Wait for desactivation before displaying next row
  wait(4300);

  // Update row display
  pulse_LAT();

  // Activate selected row
  activate_row(row);
}

// Clear bank0 bits
static void init_bank0() {
  for (int i = 0; i != 24; i++) {
    send_byte(0xff, 0);
  }
  pulse_LAT();
}

void test_pixels() {
  // Sets pixels intensity from min_color_int to max_color_int, color & row
  // changes
  const uint8_t max_color_int = 10;
  const uint8_t min_color_int = 1;
  const uint8_t diff_color_int = max_color_int - min_color_int;

  rgb_color val[8];
  int row = 0;
  while (1) {
    // Blue fading
    for (int i = 0; i != 8; i++) {
      val[i].r = 0;
      val[i].g = 0;
      val[i].b = min_color_int + (i * diff_color_int) / 7;
    }
    mat_set_row(row, val);
    // Wait 0.1s
    wait(100000000);

    // Green fading
    for (int i = 0; i != 8; i++) {
      val[i].r = 0;
      val[i].g = min_color_int + (i * diff_color_int) / 7;
      val[i].b = 0;
    }
    mat_set_row(row, val);
    wait(100000000);

    // Red fading
    for (int i = 0; i != 8; i++) {
      val[i].r = min_color_int + (i * diff_color_int) / 7;
      val[i].g = 0;
      val[i].b = 0;
    }
    mat_set_row(row, val);
    wait(100000000);

    // Move to next row
    row = (row + 1) % 8;
  }
}

extern uint8_t _binary_image_raw_start;

// Reads and displays the image starting at _binary_image_raw_start
void test_static() {
  uint8_t *reader = &_binary_image_raw_start;

  // Read it,we could have casted the pointer here :(
  // 2 dim array was a bad idea, next time we will use a 1 dim one
  rgb_color image[8][8];
  for (int row = 0; row != 8; row++) {
    for (int col = 0; col != 8; col++) {
      image[row][col].r = *reader++;
      image[row][col].g = *reader++;
      image[row][col].b = *reader++;
    }
  }

  // Display the image we just read
  int row_display = 0;
  while (1) {
    mat_set_row(row_display, image[row_display]);
    row_display = (row_display + 1) % 8;
  }
}

// displays the frame pointed by frame
void display_frame() {
  // Display loop
  int row_display = 0;
  while (1) {
    mat_set_row(row_display, &(frame[row_display * 8]));
    row_display = (row_display + 1) % 8;
  }
}

void matrix_init() {
  // Start clocks for GPIOA, B and C
  SET_BIT(RCC->AHB2ENR,
          RCC_AHB2ENR_GPIOCEN | RCC_AHB2ENR_GPIOBEN | RCC_AHB2ENR_GPIOAEN);
  // Set output pins to high speed mode
  MODIFY_REG(GPIOA->OSPEEDR, 0,
             GPIO_OSPEEDR_OSPEED2_Msk | GPIO_OSPEEDR_OSPEED3_Msk |
                 GPIO_OSPEEDR_OSPEED4_Msk | GPIO_OSPEEDR_OSPEED5_Msk |
                 GPIO_OSPEEDR_OSPEED6_Msk | GPIO_OSPEEDR_OSPEED7_Msk |
                 GPIO_OSPEEDR_OSPEED15_Msk);

  MODIFY_REG(GPIOB->OSPEEDR, 0, GPIO_OSPEEDR_OSPEED0_Msk |
                                    GPIO_OSPEEDR_OSPEED1_Msk |
                                    GPIO_OSPEEDR_OSPEED2_Msk);

  MODIFY_REG(GPIOC->OSPEEDR, 0, GPIO_OSPEEDR_OSPEED3_Msk |
                                    GPIO_OSPEEDR_OSPEED4_Msk |
                                    GPIO_OSPEEDR_OSPEED5_Msk);
  // Driver pins to output mode
  MODIFY_REG(GPIOA->MODER, GPIO_MODER_MODE2_Msk | GPIO_MODER_MODE3_Msk |
                               GPIO_MODER_MODE4_Msk | GPIO_MODER_MODE5_Msk |
                               GPIO_MODER_MODE6_Msk | GPIO_MODER_MODE7_Msk |
                               GPIO_MODER_MODE15_Msk,
             GPIO_MODER_MODE2_0 | GPIO_MODER_MODE3_0 | GPIO_MODER_MODE4_0 |
                 GPIO_MODER_MODE5_0 | GPIO_MODER_MODE6_0 | GPIO_MODER_MODE7_0 |
                 GPIO_MODER_MODE15_0);

  MODIFY_REG(GPIOB->MODER,
             GPIO_MODER_MODE0_Msk | GPIO_MODER_MODE1_Msk | GPIO_MODER_MODE2_Msk,
             GPIO_MODER_MODE0_0 | GPIO_MODER_MODE1_0 | GPIO_MODER_MODE2_0);

  MODIFY_REG(GPIOC->MODER,
             GPIO_MODER_MODE3_Msk | GPIO_MODER_MODE4_Msk | GPIO_MODER_MODE5_Msk,
             GPIO_MODER_MODE3_0 | GPIO_MODER_MODE4_0 | GPIO_MODER_MODE5_0);

  // Define reset values (cf énoncé)
  RST(0);

  LAT(1);

  SB(1);

  SCK(0);
  SDA(0);

  deactivate_rows();

  // Wait 100 ms
  wait(100 * 1000000);

  RST(1);

  // Clear bank0
  init_bank0();
}
