#include "uart.h"
#include "matrix.h"
#include "stm32l475xx.h"
#include "stm32l4xx.h"
#include <stddef.h>
#include <stdint.h>

void uart_init(uint32_t bauds) {
  // Start Clock for GPIOB
  SET_BIT(RCC->AHB2ENR, RCC_AHB2ENR_GPIOBEN);

  // MODER PB6 and PB7 of PORT B for USART1_TX and USART1_RX
  MODIFY_REG(GPIOB->MODER, GPIO_MODER_MODE7_Msk, GPIO_MODER_MODE7_1);
  MODIFY_REG(GPIOB->MODER, GPIO_MODER_MODE6_Msk, GPIO_MODER_MODE6_1);

  // AF7 is the fuction we need (0111)
  MODIFY_REG(GPIOB->AFR[0], GPIO_AFRL_AFSEL6_Msk,
             GPIO_AFRL_AFSEL6_0 | GPIO_AFRL_AFSEL6_1 | GPIO_AFRL_AFSEL6_2);
  MODIFY_REG(GPIOB->AFR[0], GPIO_AFRL_AFSEL7_Msk,
             GPIO_AFRL_AFSEL7_0 | GPIO_AFRL_AFSEL7_1 | GPIO_AFRL_AFSEL7_2);

  // USART1 Clock
  SET_BIT(RCC->APB2ENR, RCC_APB2ENR_USART1EN);

  // USART1 clock is PCLK
  CLEAR_BIT(RCC->CCIPR, RCC_CCIPR_USART1SEL);

  // Reset du port serie
  SET_BIT(RCC->APB2RSTR, RCC_APB2RSTR_USART1RST);
  CLEAR_BIT(RCC->APB2RSTR, RCC_APB2RSTR_USART1RST);

  // In case of oversampling by 16
  USART1->BRR = (uint16_t)(80000000 / bauds);

  // Set oversampling to 16; M1/M0 for 8 bit words, PCE for no parity control
  // (value is 0)
  // RXNEIE to 1 to enable IRQ
  USART1->CR1 = USART_CR1_RXNEIE;

  // 00 is for 1 bit stop
  USART1->CR2 = 0;

  // Enable irq
  // 37 44 settable USART1 USART1 global interrupt 0x0000 00D4
  NVIC_EnableIRQ(37);

  // Start USART1
  SET_BIT(USART1->CR1, USART_CR1_UE);

  // Start transmitter & Receiver
  SET_BIT(USART1->CR1, USART_CR1_TE | USART_CR1_RE);

  // Check if TE and RE are started
  while (!READ_BIT(USART1->ISR, USART_ISR_TEACK)) {
  }

  while (!READ_BIT(USART1->ISR, USART_ISR_REACK)) {
  }
}

void uart_putchar(uint8_t c) {
  // Wait for previous character
  while (!READ_BIT(USART1->ISR, USART_ISR_TXE)) {
  }

  // Write it
  WRITE_REG(USART1->TDR, c);
}

uint8_t uart_getchar() {
  // Wait until there is sth to read
  while (!READ_BIT(USART1->ISR, USART_ISR_RXNE)) {
  }

  // Read it
  int32_t value = READ_REG(USART1->RDR);

  // Cast it, no parity check
  return (uint8_t)value;
}

void uart_puts(const uint8_t *s) {
  // Wait for previous transmission
  while (!READ_BIT(USART1->ISR, USART_ISR_TC)) {
  }

  int i = 0;
  // send chars
  while (s[i]) {
    uart_putchar(s[i++]);
  }
  uart_putchar('\r');
  uart_putchar('\n');
}

void uart_gets(uint8_t *s, size_t size) {
  size_t i = 0;
  while (i != size - 1) {
    uint8_t val = uart_getchar();
    s[i++] = val;

    // A line break
    if (val == '\r' || val == '\n') {
      break;
    }
  }
  s[i] = 0;
}

void USART1_IRQHandler() {
  // Make some space to receive data
  static uint8_t receiving_init[3 * 64];
  static uint8_t *receiving = receiving_init;

  // Received data index
  static int frame_index = 0;

  // FE Error/ORE Error; clear flag
  // Index 3*65 means that the frame will not be displayed
  if (READ_BIT(USART1->ISR, USART_ISR_FE)) {
    SET_BIT(USART1->ICR, USART_ICR_FECF);
    frame_index = 3 * 65;
  }

  if (READ_BIT(USART1->ISR, USART_ISR_ORE)) {
    SET_BIT(USART1->ICR, USART_ICR_ORECF);
    frame_index = 3 * 65;
  }

  // get data
  uint8_t val = uart_getchar();

  // New frame
  if (val == 0xff) {
    // Previous frame is complete
    if (frame_index == 3 * 64) {
      // switch pointers
      uint8_t *temp = (uint8_t *)frame;
      frame = (rgb_color *)receiving;
      receiving = temp;
    }

    // reset index
    frame_index = 0;
  } else {
    if (frame_index < 3 * 64) {
      // Update received data
      receiving[frame_index] = val;
    }

    frame_index++;
  }
}
