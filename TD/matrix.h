#ifndef MATRIX_H
#define MATRIX_H
#include <stdint.h>

void matrix_init(void);
void test_pixels(void);
void test_static(void);
void display_frame(void);

// Rgb struct, should not be optimized
typedef struct {
  uint8_t r;
  uint8_t g;
  uint8_t b;
} rgb_color;

// Pointer to the displayed frame
extern volatile rgb_color *frame;
#endif
