#include <stdint.h>
extern uint8_t _bss_start, _bss_end;

__attribute__((section(".init"))) void init_bss() {
  // Set BSS to 0 (cf slideshow from SE203)
  for (uint8_t *dst = &_bss_start; dst < &_bss_end; dst++)
    *dst = 0;
}

// Copies from LMA_start to [VMA_start; VMA_end]. Size is determined by the
// difference between the two VMA
__attribute__((section(".init"))) static void
memcp(uint8_t *VMA_start, uint8_t *VMA_end, uint8_t *LMA_start) {
  uint8_t *src = LMA_start;
  uint8_t *dst = VMA_start;
  while (dst < VMA_end) {
    *dst++ = *src++;
  }
}

extern uint8_t _data_VMA_start, _data_VMA_end, _text_VMA_start, _text_VMA_end,
    _vectortable_VMA_start, _vectortable_VMA_end, _data_LMA_start,
    _text_LMA_start, _vectortable_LMA_start;

// Copies .vectortable, .data and .text section from flash to sram
__attribute__((section(".init"))) void init_ram() {
  // Copy vector table
  memcp(&_vectortable_VMA_start, &_vectortable_VMA_end,
        &_vectortable_LMA_start);

  // Copy data section
  memcp(&_data_VMA_start, &_data_VMA_end, &_data_LMA_start);

  // Copy text section
  memcp(&_text_VMA_start, &_text_VMA_end, &_text_LMA_start);
}
