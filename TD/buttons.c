#include "buttons.h"
#include "led.h"
#include "stm32l475xx.h"
#include "stm32l4xx.h"
#include <stdint.h>

void button_init() {
  // Activate GPIOC clock
  SET_BIT(RCC->AHB2ENR, RCC_AHB2ENR_GPIOCEN);

  // Pin C13 input mode (00)
  CLEAR_BIT(GPIOC->MODER, GPIO_MODER_MODE13_Msk);

  // Setup the corresponding mask bit in the EXTI_IMR register
  SET_BIT(EXTI->IMR1, EXTI_IMR1_IM13);

  // Configure the Trigger Selection bits of the Interrupt line (EXTI_RTSR and
  // EXTI_FTSR).
  // Falling edge
  CLEAR_BIT(EXTI->RTSR1, EXTI_RTSR1_RT13);
  SET_BIT(EXTI->FTSR1, EXTI_FTSR1_FT13);

  // Enable button IRQ
  // 40 47 settable EXTI15_10 EXTI Line[15:10] interrupts 0x0000 00E0
  NVIC_EnableIRQ(40);

  // PC13 as IRQ input for EXTI13 (register SYSCFG_EXTICRn, cf page 403)
  MODIFY_REG(SYSCFG->EXTICR[3], SYSCFG_EXTICR4_EXTI13_Msk,
             SYSCFG_EXTICR4_EXTI13_PC);
}

void EXTI15_10_IRQHandler() {
  // Toggle memory for led
  static int b = 0;
  SET_BIT(EXTI->PR1, EXTI_PR1_PIF13);
  b = ~b;

  // Toggle red led
  // We assume that leds have been initialized before
  if (b) {
    led_g_on();
  } else {
    led_g_off();
  }
}
