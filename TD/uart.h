#ifndef UART_H
#define UART_H
#include <stddef.h>
#include <stdint.h>

void uart_init();

void uart_putchar(uint8_t);
uint8_t uart_getchar(void);
void USART1_IRQHandler(void);
void uart_puts(const uint8_t *);
void uart_gets(uint8_t *, size_t);
#endif
