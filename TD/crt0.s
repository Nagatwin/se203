.syntax unified
.thumb
.cpu cortex-m4
.global _start
.thumb_func
.section .init

_start:
	ldr r1,=stack
	mov sp,r1

	@Zero BSS
	bl init_bss

	@Copies sections to ram
	bl init_ram

	bl main

_exit:
	b .
