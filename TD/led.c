#include "led.h"
#include "stm32l475xx.h"
#include "stm32l4xx.h"
#include <stdint.h>

void led_init() {
  // Start clocks for GPIOB and C
  SET_BIT(RCC->AHB2ENR, RCC_AHB2ENR_GPIOCEN | RCC_AHB2ENR_GPIOBEN);

  // Set Pin B14 To output mode
  MODIFY_REG(GPIOB->MODER, GPIO_MODER_MODE14_Msk, GPIO_MODER_MODE14_0);
}

// Switches on the green led
void led_g_on() { SET_BIT(GPIOB->BSRR, GPIO_BSRR_BS14); }

// Switches off the green led
void led_g_off() { SET_BIT(GPIOB->BSRR, GPIO_BSRR_BR14); }

// Switch leds blue and yellow depending on the state_t s. Cf enum in header
void led(state_t s) {
  switch (s) {
  case LED_OFF:
    // Switch off leds
    CLEAR_BIT(GPIOC->MODER, GPIO_MODER_MODE9);
    break;

  case LED_YELLOW:
    // switch off blue, and on yellow
    MODIFY_REG(GPIOC->MODER, GPIO_MODER_MODE9_Msk, GPIO_MODER_MODE9_0);
    SET_BIT(GPIOC->BSRR, GPIO_BSRR_BS9);
    break;

  case LED_BLUE:
    // switch off yellow and on blue
    MODIFY_REG(GPIOC->MODER, GPIO_MODER_MODE9_Msk, GPIO_MODER_MODE9_0);
    SET_BIT(GPIOC->BSRR, GPIO_BSRR_BR9);
    break;
  }
}
