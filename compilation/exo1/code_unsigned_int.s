	.cpu arm7tdmi
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 1
	.eabi_attribute 30, 4
	.eabi_attribute 34, 0
	.eabi_attribute 18, 4
	.file	"code.c"
	.text
	.align	2
	.global	f
	.syntax unified
	.arm
	.fpu softvfp
	.type	f, %function
f:
	@ Function supports interworking.
	@ Naked Function: prologue and epilogue provided by programmer.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r4, #0
	ldr	r5, .L4
.L2:
	ldr	r3, [r5]
	cmp	r4, r3
	bls	.L3
.L3:
	bl	g
	add	r4, r4, #1
	b	.L2
.L5:
	.align	2
.L4:
	.word	a
	.size	f, .-f
	.comm	a,4,4
	.ident	"GCC: (15:6.3.1+svn253039-1build1) 6.3.1 20170620"
