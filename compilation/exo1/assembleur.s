START:
  @On initialise les registres pour a et i
  mov	r1, #0
  ldr	r3, =a

TEST:
  @On charge la valeur de a dans r2
  ldr	r2, [r3]
  @On fait le test
  cmp	r1, r2
  bhi	END

LOOP:
  @On branch sur g
  bl	g
  @On incrémente i
  add	r1, r1, #1
  @On 'cast' i en uint8
  and	r4, r4, #255
  b TEST

END:
  b END
