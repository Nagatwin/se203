@Init des registres pour a et b
ldr	r1, =a
ldr	r2, =c

@On Récupère les adresses cases pointées
ldr	r1, [r1]
ldr	r2, [r2]

@On récupère les valeurs
ldr	r3, [r1]
ldr	r4, [r2]

@On ajoute *a+*c
add	r3, r3, r4

@On sauvegarde en mémoire
str	r3, [r1]

@Init du registre b
ldr	r1, =b

@On récupère l'adresse de la case pointée
ldr	r1, [r1]

@On récupère les valeurs
ldr	r3, [r1]
ldr	r4, [r2]

@On ajoute
add	r3, r3, r4

@On sauvegarde en mémoire
str	r3, [r1]
