# Exercice 2
## Fichiers

- [code.c](code.c) : Le code de base
- [code.s](code.s) : Le fichier assembleur généré par le cc
- [assembleur.s](assembleur.s) : Le fichier assembleur écrit à la main

## Commentaires

On a des pointeurs, on doit relire la valeur à chaque fois que l'on en a besoin (elle pourrait avoir été modifiée par un autre thread par exemple)

On serait tenté de ne pas relire la valeur pointée par C pour la 2ème addition mais elle pourrait ne plus etre valide.
